---
# Proyectos 2020-2. Autómatas y lenguajes formales. 

## Prof: Gustavo Garzón

---

# Lista de Proyectos
1. [Doctor Autómata](#proy1)
2. [Simulador de partido de tenis con máquinas de Turing](#proy2)
3. [Cifrador de Vigenère](#proy3)
4. [Resolución de ecuaciones lineales con coeficientes enteros](#proy4)
5. [Autómata finito determinista para modelar el modo piloto automático de un coche](#proy5)
6. [Guess My Character](#proy6)
7. [AFN para minimizar exposición al Covid al viajar en el departamento de Santander](#proy7)
8. [TEST DE APTITUDES PARA ESCOGER UN POSGRADO USANDO AUTÓMATAS FINITOS NO DETERMINISTAS](#proy8)
9. [Kuntsu no Fukushu - Text Novel](#proy9)
10. [Nokia 1100](#proy10)
11. [Modelado de las rutas de Expreso Brasilia que parten desde la ciudad de valledupar](#proy11)
12. [Traductor de binario a octal y hexadecimal](#proy12)
13. [AFD PARA HACER LAS COMPRAS EN EL SUPERMERCADO](#proy13)
14. [AFD PARA UNA MAQUINA EXPENDEDORA DE ELEMENTOS DE BIOSEGURIDAD](#proy14)
15. [Optimización del consumo energético de los dispositivos en una casa por medio de un controlador](#proy15)
16. [EL JUEGO DE LA VIDA](#proy16)
17. [Programa de adivinación animal](#proy17)
18. [AFD en la verificación de palabras de una sopa de letras](#proy18)
19. [Solve the captcha](#proy19)
20. [AFD QUE DETERMINA SI UNA PERSONA TIENE COVID-19 A PARTIR DE SUS SINTOMAS](#proy20)
21. [Convertidor gramatical de oraciones simples en inglés](#proy21)
22. [Autómata para la predicción del clima](#proy22)
23. [Traductor de idioma español a código morse](#proy23)
24. [AUTÓMATA ASISTENTE PARA RUTINAS DE GIMNASIO](#proy24)
25. [Autfood](#proy25)
26. [Traductor de alfabeto inglés en minúscula a binario](#proy26)

---

## Doctor Autómata <a name="proy1"></a>

**Autores:**
**Duvan Ferney Vargas Martinez, Daniel Felipe Jaimes Blanco**

[(video)](https://www.youtube.com/watch?v=-J7AfSMktyY)

---

## Simulador de partido de tenis con máquinas de Turing <a name="proy2"></a>

**Autores:**
**Juan Duarte, Yerson Ibarra, Daniel Castellanos**

[(video)](https://www.youtube.com/watch?v=I_qcHHwigSE)

---

## Cifrador de Vigenère <a name="proy3"></a>

**Autores:**
**Daniel Alejandro Castillo Rodriguez, Camilo Andrés Moreno Pinto**

[(video)](https://www.youtube.com/watch?v=us9f95gt_pw)

---

## Resolución de ecuaciones lineales con coeficientes enteros <a name="proy4"></a>

**Autores:**
**Daniel Adrián González Buendía**

[(video)](https://www.youtube.com/watch?v=c5JjdnvAVOQ)

---

## Autómata finito determinista para modelar el modo piloto automático de un coche <a name="proy5"></a>

**Autores:**
**Cristian David Fuentes Duarte**

[(video)](https://www.youtube.com/watch?v=iy5H8360CS4)

---

## Guess My Character <a name="proy6"></a>

**Autores:**
**Jefrey Steven Torres Garzón**

[(video)](https://www.youtube.com/watch?v=oe4CFPPgWJ0)

---

## AFN para minimizar exposición al Covid al viajar en el departamento de Santander <a name="proy7"></a>

**Autores:**
**Luis Felipe Cruz Ceballos, Luigi orlando garcia Duarte, Juan Sebastian Pabon Mojica**

[(video)](https://www.youtube.com/watch?v=rf7IuEoSg7E)

---

## TEST DE APTITUDES PARA ESCOGER UN POSGRADO USANDO AUTÓMATAS FINITOS NO DETERMINISTAS <a name="proy8"></a>

**Autores:**
**YOUSSEF SAID ZAMBRANO PINILLA, ADRIANA MARCELA CUJIA REYES, IVÁN DARÍO CÁRDENAS VELÁSQUEZ**

[(video)](https://www.youtube.com/watch?v=9R13sQT4d2o)

---

## Kuntsu no Fukushu - Text Novel <a name="proy9"></a>

**Autores:**
**Juan Diego Esteban Parra, David Alexander Vasquez Vivas, Jose Fredy Navarro Motta**

[(video)](https://www.youtube.com/watch?v=G12l-MJypXA)

---

## Nokia 1100 <a name="proy10"></a>

**Autores:**
**Jhon Uribe Gonzalez, Nicolas Velandia Lopez, Henry Dario Mantilla Claro**

[(video)](https://www.youtube.com/watch?v=xSrp937BenU)

---

## Modelado de las rutas de Expreso Brasilia que parten desde la ciudad de valledupar <a name="proy11"></a>

**Autores:**
**Luis Alfredo Redondo Vilches**

[(video)](https://www.youtube.com/watch?v=Rv-YXLxAR7k)

---

## Traductor de binario a octal y hexadecimal <a name="proy12"></a>

**Autores:**
**Leider Eduardo Diaz Martinez, David Mauricio Gallo Franco, Fabian Andres Bonilla Gonzalez**

[(video)](https://www.youtube.com/watch?v=i5jrALVwvPI)

---

## AFD PARA HACER LAS COMPRAS EN EL SUPERMERCADO <a name="proy13"></a>

**Autores:**
**Jhordanth Fabian Villamizar, Jeziu Danilo Moreno Gonzalez**

[(video)](https://www.youtube.com/watch?v=w7In1BmicM)

---

## AFD PARA UNA MAQUINA EXPENDEDORA DE ELEMENTOS DE BIOSEGURIDAD <a name="proy14"></a>

**Autores:**
**CARLOS IVAN RAMIREZ DIAZ**

[(video)](https://www.youtube.com/watch?v=LVrleSkLjVA)

---

## Optimización del consumo energético de los dispositivos en una casa por medio de un controlador <a name="proy15"></a>

**Autores:**
**Juan José Prenss Castro, Camilo Ciro Orozco**

[(video)](https://www.youtube.com/watch?v=LkzIJk0ebXI)

---

## EL JUEGO DE LA VIDA <a name="proy16"></a>

**Autores:**
**Andres Felipe Valenzuela Pardo, Adriana Villamizar Vera, Rubi Tatiana Utima Echeverry**

[(video)](https://www.youtube.com/watch?v=BrlnwaZl46I)

---

## Programa de adivinación animal <a name="proy17"></a>

**Autores:**
**Alejandro Rodríguez Vargas, Juan Pablo Ramírez Duarte, Jhon Jaime Novoa Martínez**

[(video)](https://www.youtube.com/watch?v=NhjhTljbwuM)

---

## AFD en la verificación de palabras de una sopa de letras <a name="proy18"></a>

**Autores:**
**Miguel Daniel Velandia Pinilla**

[(video)](https://www.youtube.com/watch?v=sbaWl9oig38)

---

## Solve the captcha <a name="proy19"></a>

**Autores:**
**Andrea Juliana Urrego Paredes, Miguel Felipe Espinel Botero, Juan Sebastián Duran Macias**

[(video)](https://www.youtube.com/watch?v=59RnBhLLuno)

---

## AFD QUE DETERMINA SI UNA PERSONA TIENE COVID-19 A PARTIR DE SUS SINTOMAS <a name="proy20"></a>

**Autores:**
**Jose Fabian Jimenez Ovalle, Geydi Dayana Calderón Salazar, Helman Andres Merchán Quevedo**

[(video)](https://www.youtube.com/watch?v=TA6NQ62a80c)

---

## Convertidor gramatical de oraciones simples en inglés <a name="proy21"></a>

**Autores:**
**Juan Esteban Castillo Orduz**

[(video)](https://www.youtube.com/watch?v=O5rBNpnDP8I)

---

## Autómata para la predicción del clima <a name="proy22"></a>

**Autores:**
**Cristhian Rafael Lizcano Arenas, Manuel Vicenzy Roa Prada, Álvaro Javier Villamizar Sánchez**

[(video)](https://www.youtube.com/watch?v=kyU_W9qZ73I)

---

## Traductor de idioma español a código morse <a name="proy23"></a>

**Autores:**
**Edinsson Gutiérrez, Amin Barbosa**

[(video)](https://www.youtube.com/watch?v=tO3lyUiUObs)

---

## AUTÓMATA ASISTENTE PARA RUTINAS DE GIMNASIO <a name="proy24"></a>

**Autores:**
**Hernan Santiago Vallejo Garcia**

[(video)](https://www.youtube.com/watch?v=HCkkV-I7qxA)

---

## Autfood <a name="proy25"></a>

**Autores:**
**Javier Fernando Carvajal Sanabria, Esneydith Patricia Jaimes Benitez, Juan Sebastian Vanegas Rico**

[(video)](https://www.youtube.com/watch?v=T5sH7yDeKeE)

---

## Traductor de alfabeto inglés en minúscula a binario <a name="proy26"></a>

**Autores:**
**Jose Isaac Leal Manosalva, Jean Carlo Rodriguez Sanchez**

[(video)](https://www.youtube.com/watch?v=q4520fVHPGQ)

---
