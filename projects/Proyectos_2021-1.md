---
# Proyectos 2021-1. Autómatas y lenguajes formales. 

## Prof: Gustavo Garzón

---

# Lista de Proyectos
1. [Automata para el procesamiento de la elaboración del whisky](#proy1)
2. [Encuentra tu hotel](#proy2)
3. [Can I buy it](#proy3)
4. [Metrolinea routes](#proy4)
5. [AFD para la clasificación de basuras](#proy5)

---

## Automata para el procesamiento de la elaboración del whisky <a name="proy1"></a>

**Autores:**
**Julian Leon, Kevin Morales**

[(video)](https://www.youtube.com/watch?v=IRIZvVj2ws)
[(repositorio)](https://github.com/Kevs99/AUTOMATA-PARA-EL-PROCESAMIENTO-DE-LA-ELABORACI-N-DEL-WHISKY)

---

## Encuentra tu hotel <a name="proy2"></a>

**Autores:**
**Juan Sebastián Pabón Mójica, Kevin Alexis Prada Morales**

[(video)](https://www.youtube.com/watch?v=3VaqVIOIqrE)
[(repositorio)](https://gitlab.com/kevinprada0101/encuentra-tu-hotel-proyecto)

---

## Can I buy it <a name="proy3"></a>

**Autores:**
**Brayan Ferney Uribe Rodríguez, Fabian Ernesto Hernández Ramírez, Santiago Quintero Jaimes**

[(video)](https://www.youtube.com/watch?v=Ar0zjrvRJC4)
[(repositorio)](https://github.com/MagicAedo/Proyecto-Autonomas---CAN-I-BUY-IT)

---

## Metrolinea routes <a name="proy4"></a>

**Autores:**
**Gabriel Fernando Reyes Guevara, Diego Alejandro Lopez Camacho, Juan Sebastian Espinosa Espinosa**

[(video)](https://www.youtube.com/watch?v=vQOfOx-ATIA)
[(repositorio)](https://github.com/diegolopezrm/ProyectoMetrolineaRutes)

---

## AFD para la clasificación de basuras <a name="proy5"></a>

**Autores:**
**Johann Sleyder Contreras Rojas, Yuly Stefanny Bohorquez Bohorquez, Juan Felipe Velandia Naranjo**

[(video)](https://gitlab.com/juanfelipevn/proyecto-automatas/-/raw/main/Informacion_Complementaria_-_Video.mp4?inline=false)
[(repositorio)](https://gitlab.com/juanfelipevn/proyecto-automatas)

---
